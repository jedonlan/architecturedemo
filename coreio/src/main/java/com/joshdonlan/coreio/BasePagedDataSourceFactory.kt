package com.joshdonlan.coreio

import androidx.paging.DataSource
import androidx.paging.PagedList

abstract class BasePagedDataSourceFactory<MODEL: Any>: DataSource.Factory<Int, MODEL>() {

    val config by lazy { initConfig() }

    /**
     * Establish parameters for paged loading.
     * @param initLoadSize - Refers to [PagedList.Config.initialLoadSizeHint]
     * @param pageSize - Refers to [PagedList.Config.pageSize]
     * @param prefetch - Refers to [PagedList.Config.prefetchDistance]
     * @param placeHolders - Refers to [PagedList.Config.enablePlaceholders]
     */
    open fun initConfig(initLoadSize: Int = DEFAULT_LOAD_SIZE, pageSize: Int = DEFAULT_PAGE_SIZE, prefetch: Int = DEFAULT_PREFETCH, placeHolders: Boolean = DEFAULT_PLACEHOLDERS): PagedList.Config {
        return buildConfig(initLoadSize, pageSize, prefetch, placeHolders)
    }

    private fun buildConfig(initLoadSize: Int, pageSize: Int, prefetch: Int, placeHolders: Boolean): PagedList.Config {
        return PagedList.Config.Builder()
            .setEnablePlaceholders(placeHolders)
            .setInitialLoadSizeHint(initLoadSize)
            .setPageSize(pageSize)
            .setPrefetchDistance(prefetch)
            .build()
    }

    companion object {
        const val DEFAULT_LOAD_SIZE = 40
        const val DEFAULT_PAGE_SIZE = 20
        const val DEFAULT_PREFETCH = 4
        const val DEFAULT_PLACEHOLDERS = true
    }

}