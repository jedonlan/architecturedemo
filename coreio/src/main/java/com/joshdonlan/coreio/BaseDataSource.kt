package com.joshdonlan.coreio

import retrofit2.Response

abstract class BaseDataSource {

    protected suspend fun <T> execute(call: suspend() -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                response.body()?.also {
                    return Resource.success(it)
                }
            }
            return error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error("API call failed for the following reason: $message")
    }

}