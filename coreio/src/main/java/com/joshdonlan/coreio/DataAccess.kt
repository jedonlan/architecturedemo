package com.joshdonlan.coreio

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers

object DataAccess {

    fun <T,A> fetch (
        dbQuery: () -> LiveData<T>,
        networkCall: suspend () -> Resource<A>,
        saveCallResult: suspend (A) -> Unit
    ): LiveData<Resource<T>> = liveData(Dispatchers.IO) {

            emit(Resource.loading())

            val source = dbQuery.invoke().map { Resource.success(it) } // Database pulls will always succeed
            emitSource(source)

            val responseStatus = networkCall.invoke()
            if (responseStatus.status == Resource.Status.SUCCESS) {
                responseStatus.data?.also {
                    saveCallResult(it)
                }

            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message ?: ""))
                emitSource(source)
            }

        }

}