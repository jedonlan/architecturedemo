package com.joshdonlan.coreio

import androidx.lifecycle.asFlow
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import retrofit2.Response

abstract class BasePagedRemoteDataSource<RESULT: Any, MODEL: Any>(private val scope: CoroutineScope): PageKeyedDataSource<Int, MODEL>() {

    val pagedListFlow by lazy { createLiveData(factory) }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MODEL>) {
        scope.launch(Dispatchers.IO) {
            fetchInitial().also { response ->
                response.data.data?.also { data ->
                    storeResults(data)
                    callback.onResult(data, response.previous, response.next)
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MODEL>) {
        scope.launch(Dispatchers.IO) {
            fetchBefore(params.key).also { response ->
                response.data.data?.also { data ->
                    storeResults(data)
                    callback.onResult(data, response.previous)
                }
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MODEL>) {
        scope.launch(Dispatchers.IO) {
            fetchAfter(params.key).also { response ->
                response.data.data?.also { data ->
                    storeResults(data)
                    callback.onResult(data, response.next)
                }
            }
        }
    }

    private fun createLiveData(factory: com.joshdonlan.coreio.BasePagedDataSourceFactory<MODEL>): Flow<PagedList<MODEL>> {
        return LivePagedListBuilder(factory, factory.config)
            .build().asFlow()
    }

    protected suspend fun execute(call: suspend() -> Response<RESULT>): PagedResponse<MODEL> {
        try {
            val response = call()
            if (response.isSuccessful) {
                response.body()?.also {
                    return buildPagedResponse(Resource.success(it))
                }
            }
            return PagedResponse(error("${response.code()} ${response.message()}"), null, null)
        } catch (e:Exception) {
            return PagedResponse(error(e.message ?: e.toString()), null, null)
        }
    }

    private fun error(message: String): Resource<List<MODEL>> {
        return Resource.error("API call failed for the following reason: $message")
    }

    /**
     * Data class for API call result return.
     * @param data - API results in List<MODEL> form.
     * @param previous - previous paginated call. null for NA
     * @param next - next paginated call. null for NA
     */
    data class PagedResponse<MODEL> (
        val data: Resource<List<MODEL>>,
        val previous: Int?,
        val next: Int?
    )

    abstract val dataSourceName: String

    abstract val factory: com.joshdonlan.coreio.BasePagedDataSourceFactory<MODEL>

    abstract suspend fun fetchInitial(): PagedResponse<MODEL>
    abstract suspend fun fetchBefore(page: Int): PagedResponse<MODEL>
    abstract suspend fun fetchAfter(page: Int): PagedResponse<MODEL>

    open suspend fun storeResults(data: List<MODEL>) {}

    abstract fun buildPagedResponse(resource: Resource<RESULT>): PagedResponse<MODEL>

}