package com.joshdonlan.core

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.core.lifecycle.StateBundle
import java.lang.Exception
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

/**
 * Abstract ViewModel for use with MVI
 * @param VIEW - common interface with associated View such as [BaseActivity]
 */
abstract class BaseViewModel<VIEW: Any> : ViewModel(), BasePresenter<VIEW> {

    override lateinit var view: VIEW

    @Suppress("UNCHECKED_CAST")
    final override val nullView: VIEW by lazy { ProxyView.createProxy(this::class.findAnnotation<WithView>()?.value as KClass<VIEW>) }

    /**
     * Collection of child presenters.
     */
    private val delegatePresenters = mutableListOf<DelegatePresenter<VIEW>>()

    private var hasPresented = false

    @CallSuper
    override fun present(bundle: StateBundle) {
        if (!hasPresented) {
            presentOnce(bundle)
        }
        presentAlways(bundle)
        hasPresented = true
    }

    /**
     * These commands will only be presented once regardless how many times @see[present] is called.
     */
    open fun presentOnce(bundle: StateBundle) {}

    /**
     * These commands will only be presented every time @see[present] is called.
     */
    open fun presentAlways(bundle: StateBundle) {}

    /**
     * Adds a delegate to internal collection for automation.
     * @param delegatePresenter - [DelegatePresenter] to be added.
     */
    fun addDelegatePresenter(delegatePresenter: DelegatePresenter<VIEW>) {
        delegatePresenters.add(delegatePresenter)
    }

    /**
     * Verifies delegate is being tracked in collection.
     * @param delegatePresenter - [DelegatePresenter] to be verified.
     */
    fun hasDelegatePresenter(delegatePresenter: DelegatePresenter<*>): Boolean {
        return delegatePresenters.contains(delegatePresenter)
    }

    @CallSuper
    override fun attach(view: VIEW) {
        this.view = view
        viewAttached()
    }

    @CallSuper
    override fun attachDelegates(view: VIEW) {
        delegatePresenters.forEach {
            it.attach(view)
        }
    }

    @CallSuper
    override fun detach() {
        delegatePresenters.forEach {
            it.detach()
        }
        this.view = nullView
        viewWillDetach()
    }

    override fun onBackPressed(): Boolean {
        for (delegate in delegatePresenters) {
            if (delegate.onBackPressed()) {
                return true
            }
        }
        return false
    }

    /**
     * Tasks that should run automatically once view is attached should be
     * called within [viewAttached]
     */
    @CallSuper
    open fun viewAttached() {}

    /**
     * Tasks and cleanup to be called prior to view detaching.
     */
    @CallSuper
    open fun viewWillDetach() {}

    override fun saveState(bundle: Bundle): Bundle {
        return bundle
    }

    override fun restoreState(bundle: Bundle?) {}

    override fun onResume() {}

    override fun onPause() {}

    override fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    override fun handlePermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {}

    override fun handleNewIntent(bundle: Bundle?) {}

    override fun handleConfigurationChanged(configuration: Configuration) {}

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return false
    }

    override fun onOptionsMenuCreated(menu: Menu?) {}

    private fun initView() {
        try {
            view = nullView
        } catch (e: Exception) {
            throw IllegalStateException("'WithView' annotation must be specified.")
        }
    }

    init {
        initView()
    }

}