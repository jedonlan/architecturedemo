package com.joshdonlan.core.annotation

import androidx.annotation.Keep
import com.joshdonlan.core.BaseViewModel
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

@Inherited
@Keep
@Retention(AnnotationRetention.RUNTIME)
annotation class WithViewModel(val value: KClass<out BaseViewModel<*>>)