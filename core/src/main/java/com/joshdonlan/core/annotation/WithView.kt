package com.joshdonlan.core.annotation

import androidx.annotation.Keep
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

@Inherited
@Keep
@Retention(AnnotationRetention.RUNTIME)
annotation class WithView(val value: KClass<*>)
