package com.joshdonlan.core.annotation

import androidx.annotation.Keep
import com.joshdonlan.core.di.InjectFactory
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

@Inherited
@Keep
@Retention(AnnotationRetention.RUNTIME)
annotation class WithPresenterFactory(val value: KClass<out InjectFactory<*>>)
