package com.joshdonlan.core

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.joshdonlan.core.lifecycle.*

/**
 * Abstract Activity for use with MVI.
 * @param VIEW - interface of View functions that should be available to the [PRESENTER]
 * @param PRESENTER - strictly typed BasePresenter such as Delegate or ViewModel
 */
abstract class BaseActivity<VIEW: Any, PRESENTER: BasePresenter<VIEW>>
    : AppCompatActivity() {

    /**
     * Value to reference WithViewModel [BaseViewModel]. Injection supported.
     * Assigned as override val viewModel: <PRESENTER> by viewModels()
     * If activity is using WithPresenterFactory, set to null.
     */
    abstract val viewModel: BaseViewModel<VIEW>?

    /**
     * Value to hold reference to ViewBinding generated in [initViewBinding], should be cast to
     * specific ViewBinding class for Activity.
     */
    abstract val binding: ViewBinding

    /**
     * Method to inflate the ViewBinding an return value to be stored in [binding]
     */
    abstract fun initViewBinding(): ViewBinding

    /**
     * Value for menu resource id
     */
    private val menuResource by lazy { declareMenuResource() }

    /**
     * OPTIONAL - declare a menu resource id for Android Toolbar
     */
    open fun declareMenuResource(): MenuResource {
        return MenuResource()
    }

    private val activityLifecycle by lazy { createLifecycleHandler() }

    private fun createLifecycleHandler(): BaseLifeCycleHander<VIEW, PRESENTER> {
        viewModel?.also {
            return ViewModelLifecycleHandler(this, menuResource, it)
        }
        return PresenterLifecycleHandler(this, menuResource)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        activityLifecycle.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        activityLifecycle.onResume()
    }

    override fun onPause() {
        super.onPause()
        activityLifecycle.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        activityLifecycle.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        activityLifecycle.onRequestPermissionResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activityLifecycle.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (activityLifecycle.onOptionsItemSelected(item)) {
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return if (activityLifecycle.onCreateOptionsMenu(menu)) {
            true
        } else {
            super.onCreateOptionsMenu(menu)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        activityLifecycle.onConfigurationChanged(newConfig)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        activityLifecycle.onNewIntent(intent)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        activityLifecycle.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        activityLifecycle.onRestoreInstanceState(savedInstanceState)
    }

}