package com.joshdonlan.core.di

/**
 * Dependency Injection wrapper.
 * @param T - object to be initialized
 */
abstract class InjectFactory<T: Any> {

    lateinit var item: T

    /**
     * Method that establishes the object to be built. If desired, this item can be injected with DI.
     * @param item - object to be build, injectable.
     */
    abstract fun initItem(item: T)

}