package com.joshdonlan.core.di

import com.joshdonlan.core.BasePresenter
import java.lang.RuntimeException
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

object PresenterFactory {

    @Suppress("UNCHECKED_CAST")
    fun <PRESENTER: BasePresenter<*>> createPresenter(
        factory: KClass<out InjectFactory<*>>
    ): PRESENTER {

        return factory.let {
            try {
                it.createInstance().let { instance ->
                    instance.item as PRESENTER
                }
            } catch (e: Throwable) {
                throw RuntimeException(e)
            }
        }
    }

}