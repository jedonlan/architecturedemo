package com.joshdonlan.core

import android.app.Application
import timber.log.Timber

abstract class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}