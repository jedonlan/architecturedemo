package com.joshdonlan.core

import com.joshdonlan.core.util.NullInvocationHandler
import java.lang.reflect.Proxy
import kotlin.reflect.KClass

object ProxyView {

    /**
     * Creates a proxy instance for generic class.
     * @param T - Class type
     * @param targetClass - Class to be returned
     *
     */
    fun <T: Any> createProxy(targetClass: KClass<T>): T {
        try {
            val proxy = Proxy.newProxyInstance(targetClass.java.classLoader, arrayOf<Class<*>>(targetClass.java), NullInvocationHandler())
            return targetClass.java.cast(proxy)!!
        } catch (e: Exception) {
            throw IllegalStateException("Missing WithView annotation.")
        }

    }

}