package com.joshdonlan.core.lifecycle

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.joshdonlan.core.BasePresenter

interface ActivityLifecycle<VIEW: Any, PRESENTER: BasePresenter<VIEW>> {
    fun onCreate(savedInstanceState: Bundle?)
    fun onResume()
    fun onPause()
    fun onSaveInstanceState(outState: Bundle)
    fun onRestoreInstanceState(savedInstanceState: Bundle)
    fun onNewIntent(intent: Intent)
    fun onDestroy()
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    fun onOptionsItemSelected(item: MenuItem): Boolean
    fun onCreateOptionsMenu(menu: Menu): Boolean
    fun onConfigurationChanged(newConfig: Configuration)
}