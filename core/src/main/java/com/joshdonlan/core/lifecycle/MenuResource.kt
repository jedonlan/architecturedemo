package com.joshdonlan.core.lifecycle

data class MenuResource constructor(
    val id: Int = 0,
)