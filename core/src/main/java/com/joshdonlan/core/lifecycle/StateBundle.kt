package com.joshdonlan.core.lifecycle

import android.os.Bundle
import kotlinx.android.parcel.Parcelize

@Parcelize
class StateBundle constructor(
    var bundle: Bundle,
    var arguments: Bundle? = null
)