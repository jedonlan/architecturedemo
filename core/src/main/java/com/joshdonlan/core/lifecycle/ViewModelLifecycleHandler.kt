package com.joshdonlan.core.lifecycle

import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.BasePresenter
import com.joshdonlan.core.BaseViewModel

class ViewModelLifecycleHandler<VIEW: Any, PRESENTER: BasePresenter<VIEW>>(
    activity: BaseActivity<VIEW, PRESENTER>,
    menuResource: MenuResource,
    viewModel: BaseViewModel<VIEW>
): BaseLifeCycleHander<VIEW, PRESENTER>(activity, menuResource) {

    override val presenter = viewModel as PRESENTER

}