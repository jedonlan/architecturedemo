package com.joshdonlan.core.lifecycle

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.BasePresenter

abstract class BaseLifeCycleHander<VIEW: Any, PRESENTER: BasePresenter<VIEW>>(
    protected val activity: BaseActivity<VIEW, PRESENTER>,
    private val menuResource: MenuResource
): ActivityLifecycle<VIEW, PRESENTER> {

    abstract val presenter: PRESENTER

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        val stateBundle = StateBundle(savedInstanceState ?: Bundle(), activity.intent.extras)
        presenter.attach(activity as VIEW)
        presenter.present(stateBundle)
        presenter.attachDelegates(activity as VIEW)
    }

    override fun onResume() {
        presenter.onResume()
    }

    override fun onPause() {
        presenter.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        presenter.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        presenter.restoreState(savedInstanceState)
    }

    override fun onNewIntent(intent: Intent) {
        presenter.handleNewIntent(intent.extras)
    }

    override fun onDestroy() {
        presenter.detach()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.handleActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        presenter.handlePermissionResult(requestCode, permissions, grantResults)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return presenter.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return if (menuResource.id != 0) {
            activity.menuInflater.inflate(menuResource.id, menu)
            presenter.onOptionsMenuCreated(menu)
            true
        } else {
            false
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        presenter.handleConfigurationChanged(newConfig)
    }

}