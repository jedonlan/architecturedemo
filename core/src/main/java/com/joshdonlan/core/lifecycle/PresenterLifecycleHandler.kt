package com.joshdonlan.core.lifecycle

import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.BasePresenter
import com.joshdonlan.core.annotation.WithPresenterFactory
import com.joshdonlan.core.annotation.WithViewModel
import com.joshdonlan.core.di.PresenterFactory.createPresenter
import java.lang.RuntimeException
import kotlin.reflect.full.findAnnotation


class PresenterLifecycleHandler<VIEW: Any, PRESENTER: BasePresenter<VIEW>>(
    activity: BaseActivity<VIEW, PRESENTER>,
    menuResource: MenuResource
): BaseLifeCycleHander<VIEW, PRESENTER>(activity, menuResource) {

    override val presenter: PRESENTER = generatePresenter()

    private fun generatePresenter(): PRESENTER {
        activity::class.findAnnotation<WithPresenterFactory>()?.value?.also {
            return createPresenter(it) as PRESENTER
        }
        throw RuntimeException(String.format("Missing required '%s' or %s annotation.", WithPresenterFactory::class.java.simpleName, WithViewModel::class.simpleName))
    }

}