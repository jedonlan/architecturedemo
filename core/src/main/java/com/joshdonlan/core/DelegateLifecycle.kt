package com.joshdonlan.core

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

interface DelegateLifecycle {
    fun saveState(bundle: Bundle): Bundle { return Bundle() }
    fun restoreState(bundle: Bundle?) {}
    fun onResume() {}
    fun onPause() {}
    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}
    fun handlePermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {}
    fun handleNewIntent(bundle: Bundle?) {}
    fun handleConfigurationChanged(configuration: Configuration) {}
    fun onOptionsItemSelected(item: MenuItem?): Boolean { return false}
    fun onOptionsMenuCreated(menu: Menu?) {}
}