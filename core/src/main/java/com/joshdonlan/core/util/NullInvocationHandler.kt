package com.joshdonlan.core.util

import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method

class NullInvocationHandler: InvocationHandler {

    @Throws(Throwable::class)
    override fun invoke(proxy: Any?, method: Method?, args: Array<out Any>?): Any {
        return Unit
    }

}