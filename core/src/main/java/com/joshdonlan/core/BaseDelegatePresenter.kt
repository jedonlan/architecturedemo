package com.joshdonlan.core

import androidx.annotation.CallSuper
import com.joshdonlan.core.annotation.WithView
import kotlinx.coroutines.CoroutineScope
import java.lang.Exception
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

abstract class BaseDelegatePresenter<VIEW: Any>: DelegatePresenter<VIEW> {

    private val delegatePresenters = mutableListOf<DelegatePresenter<in VIEW>>()

    lateinit var view: VIEW

    private val nullView: VIEW by lazy { ProxyView.createProxy(this::class.findAnnotation<WithView>()?.value as KClass<VIEW>) }

    var coroutineScope: CoroutineScope? = null

    /**
     * If delegate needs to run coroutines, set the corresponding scope from the base delegate.
     * @param coroutineScope - the base delegate scope such as viewModelSCope
     */
    fun setScope(coroutineScope: CoroutineScope) {
        this.coroutineScope = coroutineScope
    }

    /**
     * Attaches the provided [VIEW] to the presenter and delegates.
     * @param view - interface for view methods associated with presenter.
     */
    @CallSuper
    override fun attach(view: VIEW) {
        delegatePresenters.forEach {
            it.attach(view)
        }
        this.view = view
        viewAttached()
    }

    /**
     * Detaches view from presenter and delegates.
     */
    @CallSuper
    override fun detach() {
        delegatePresenters.forEach {
            it.detach()
        }
        this.view = nullView
        viewWillDetach()
    }

    /**
     * Tasks that should run automatically once view is attached should be
     * called within [viewAttached]
     */
    open fun viewAttached() {}

    /**
     * Tasks and cleanup to be called prior to view detaching.
     */
    open fun viewWillDetach() {}

    /**
     * Propagates onBackPressed() events to all delegates of the presenter.
     * Delegates should return true if they are handling the event and do not
     * want Android to use the system back function.
     */
    override fun onBackPressed(): Boolean {
        for (delegate in delegatePresenters) {
            if (delegate.onBackPressed()) {
                return true
            }
        }
        return false
    }

    /**
     * Adds a delegate to internal collection for automation.
     * @param delegatePresenter - [DelegatePresenter] to be added.
     */
    fun addDelegatePresenter(delegatePresenter: DelegatePresenter<in VIEW>) {
        delegatePresenters.add(delegatePresenter)
    }

    /**
     * Verifies delegate is being tracked in collection.
     * @param delegatePresenter - [DelegatePresenter] to be verified.
     */
    fun hasDelegatePresenter(delegatePresenter: DelegatePresenter<*>): Boolean {
        return delegatePresenters.contains(delegatePresenter)
    }


    private fun initView() {
        try {
            view = nullView
        } catch (e: Exception) {
            throw IllegalStateException("'WithView' annotation must be specified.")
        }
    }

    init {
        initView()
    }

}