package com.joshdonlan.core

import com.joshdonlan.core.lifecycle.StateBundle

interface BasePresenter<VIEW: Any>: DelegateLifecycle {

    var view: VIEW
    val nullView: VIEW

    /**
     * Initial presentation of content called from onCreate.
     * @param bundle - arguments and extras data
     */
    fun present(bundle: StateBundle)

    /**
     * Attaches the provided [VIEW] to the presenter.
     * @param view - interface for view methods associated with presenter.
     */
    fun attach(view: VIEW)

    /**
     * Attached the delegates to the presenter.
     * @param view - interface for view methods associated with presenter.
     */
    fun attachDelegates(view: VIEW)

    /**
     * Detaches view from presenter and delegates.
     */
    fun detach()

    /**
     * Propagates onBackPressed() events to all delegates of the presenter.
     * Delegates should return true if they are handling the event and do not
     * want Android to use the system back function.
     */
    fun onBackPressed(): Boolean

}