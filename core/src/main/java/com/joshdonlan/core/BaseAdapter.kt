package com.joshdonlan.core

import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

@ExperimentalCoroutinesApi
abstract class BaseAdapter<MODEL: Any, VIEWHOLDER: BaseViewHolder<MODEL>>(protected val adapterStore: ArrayList<MODEL>?): RecyclerView.Adapter<VIEWHOLDER>() {

    val itemClicks = BroadcastChannel<MODEL>(1)

    @FlowPreview
    fun getItemClicksFlow(): Flow<MODEL> {
        return itemClicks.asFlow()
    }

    @CallSuper
    override fun onBindViewHolder(holder: VIEWHOLDER, position: Int) {
        adapterStore?.get(position).also {
            holder.bind(it)
        }
    }

    override fun getItemCount(): Int {
        return adapterStore?.size ?: 0
    }

}