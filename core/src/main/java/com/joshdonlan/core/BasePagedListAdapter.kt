package com.joshdonlan.core

import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

abstract class BasePagedListAdapter<MODEL, VIEWHOLDER: RecyclerView.ViewHolder>(diffCallback: DiffUtil.ItemCallback<MODEL>): PagedListAdapter<MODEL, VIEWHOLDER>(diffCallback) {

    @ExperimentalCoroutinesApi
    val itemClicks = BroadcastChannel<MODEL>(1)

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun getItemClicksFlow(): Flow<MODEL> {
        return itemClicks.asFlow()
    }

}