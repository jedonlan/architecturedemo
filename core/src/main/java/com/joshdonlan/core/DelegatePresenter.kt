package com.joshdonlan.core

interface DelegatePresenter<in VIEW>: DelegateLifecycle {

    /**
     * Attaches the provided [VIEW] to the presenter and delegates.
     * @param view - interface for view methods associated with presenter=
     */
    fun attach(view: VIEW)

    /**
     * Detaches view from presenter and delegates.
     */
    fun detach()

    /**
    * Propagates onBackPressed() events to all delegates of the presenter.
    * Delegates should return true if they are handling the event and do not
    * want Android to use the system back function.
    */
    fun onBackPressed(): Boolean

}
