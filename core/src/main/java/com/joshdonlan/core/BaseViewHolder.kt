package com.joshdonlan.core

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel

@ExperimentalCoroutinesApi
abstract class BaseViewHolder<MODEL: Any>(
    itemView: View
): RecyclerView.ViewHolder(itemView) {

    lateinit var item: MODEL

    @CallSuper
    open fun bind(model: MODEL?) {
        model?.also {
            this.item = model
        }
    }

    abstract fun observeClicks(channel: BroadcastChannel<MODEL>, scope: CoroutineScope)

}