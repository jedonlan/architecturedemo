# Architecture Demo - Rick and Morty Style

This repository represents an abstracted architecture for Android that attempts to separate application logic from the Android View/Lifecycle hierarchy as much as possible. It consists of a few main components...

# Activity Structure

The **core** module provides a set of base classes that can serve as a scaffold for building your application components. These components are enabled and enforced by a series of annotations for each component. 

## BaseActivity<VIEW, PRESENTER>
**VIEW** represents a generic interface that serves as the contract between the presenter and Android. This interface should contain any UI interactions that are required of the presenter such as setting a RecyclerView adapter or getting clicks from UI elements.

**PRESENTER** represents either a ViewModel or standard presenter object defined by *BaseViewModel* or *BasePresenter*. This is the primary logic container for application code. 

A *BaseActivity* should be annotated with `@WithViewModel` or `@WithPresenterFactory` that defines the presenter class extending either *BaseViewModel* or *PresenterFactory*.

The code determines which presenter model to utilize based on the overridden variable `viewModel`. If *null*, the library will try and utilize a standard presenter constructed by the factory annotation. If a viewModel is specified, the library will try and attach it appropriately. To define a viewModel, the KTX library is used as such:

    override val viewModel: MyCustomViewModel by viewModels()
   
The final required component for creating an Activity is to define the ViewBinding. This is done by overriding the `binding` value and utilizing the abstracted `initViewBinding()` method...

    override val binding by lazy { initViewBinding() as MyActivityBinding }
    
    override fun initViewBinding(): ViewBinding {
        return MyActivityBinding.inflate(layoutInflater)
    }

## BaseViewModel<VIEW>

This is the typical PRESENTER to use with Android Activities as it leverages Android lifecycle to house coroutine scope and manage persistence. The **VIEW** provided mirrors that defined in the `BaseActivity` declaration. It is important to note that any `DelegatePresenter` added to this base presenter will need this **VIEW** to extend theirs.

BaseViewModels are annotated with `@WithView` that also references the UI interface defined as the **VIEW**.

As this library uses *Hilt* for Dependency Injection, ViewModel constructors may be annotated with `@ViewModelInject ` if injection is desired.

Once annotated, this ViewModel will automatically get access and receive callbacks to the following Android lifecycle methods:

 - onCreate(savedInstanceState: Bundle?)
 - onResume()   
 - onPause()   
 - onSaveInstanceState(outState: Bundle)   
 - onRestoreInstanceState(savedInstanceState: Bundle)   
 - onNewIntent(intent: Intent)   
 - onDestroy()   
 - onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)  
 - onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)   
 - onOptionsItemSelected(item: MenuItem): Boolean   
 - onCreateOptionsMenu(menu: Menu): Boolean   
 - onConfigurationChanged(newConfig: Configuration)

The presenter will also have access to two methods which trigger in onCreate. When using ViewModels, in normal circumstances, each method will only fire once, but there are cases, such as manual destruction and creation of a view, where that might not be the case. These methods are:
 - presentOnce(bundle: StateBundle) 
 - presentAlways(bundle: StateBundle)

Regardless of how many times `present(bundle:StateBundle)` is called, `presentOnce` will only execute its logic a single time, whereas `presentAlways` will run every time. These methods occur during the **onCreate** phase of the Activity lifecycle.

Updated 12-29-2020 7:04pm EST