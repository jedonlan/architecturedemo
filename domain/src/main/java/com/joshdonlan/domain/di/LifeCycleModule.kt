package com.joshdonlan.domain.di

import android.app.Activity
import androidx.activity.ComponentActivity
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
object LifeCycleModule {

    @Provides
    fun provideLifecycleCoroutineScope(activity: Activity): LifecycleCoroutineScope = (activity as ComponentActivity).lifecycleScope

}