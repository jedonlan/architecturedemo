package com.joshdonlan.domain.di

import android.content.Context
import com.joshdonlan.domain.database.CharacterDao
import com.joshdonlan.domain.database.EpisodeDao
import com.joshdonlan.domain.database.RickAndMortyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun providesCharacterDao(@ApplicationContext appContext: Context): CharacterDao {
        return RickAndMortyDatabase.getInstance(appContext).characterDao
    }

    @Provides
    fun providesEpisodeDao(@ApplicationContext appContext: Context): EpisodeDao {
        return RickAndMortyDatabase.getInstance(appContext).episodeDao
    }

}