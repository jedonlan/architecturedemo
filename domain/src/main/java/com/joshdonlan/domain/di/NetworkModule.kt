package com.joshdonlan.domain

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.joshdonlan.domain.api.RickAndMortyClient
import com.joshdonlan.domain.api.RickAndMortyService
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.Date

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private fun createMoshiConverter(): MoshiConverterFactory {
        return Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .add(KotlinJsonAdapterFactory())
            .build()
            .let{
                MoshiConverterFactory.create(it)
            }
    }

    @Provides
    fun providesRickAndMortyService(): RickAndMortyService {
        val baseURL = "https://rickandmortyapi.com/api/"

        return Retrofit.Builder()
            .addConverterFactory(createMoshiConverter())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(RickAndMortyClient.getClient())
            .baseUrl(baseURL)
            .build()
            .create(RickAndMortyService::class.java)
    }

}

