package com.joshdonlan.domain.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class RickAndMortyClient: OkHttpClient() {

    companion object {

        private val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        fun getClient(): OkHttpClient {
            return Builder()
                .addInterceptor(logging)
                .build()
        }

    }

}