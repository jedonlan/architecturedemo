package com.joshdonlan.domain.api

import com.joshdonlan.domain.character.model.CharacterResponse
import com.joshdonlan.domain.character.model.Character
import com.joshdonlan.domain.episode.model.Episode
import com.joshdonlan.domain.episode.model.EpisodeResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickAndMortyService {

    // CHARACTERS

    @GET("character/{ids}")
    suspend fun getCharactersByIds(@Path("ids") characterIds: String): Response<List<Character>>

    @GET("character")
    suspend fun getCharacters(@Query("page") page: Int): Response<CharacterResponse>

    @GET("character/{id}")
    suspend fun getCharacter(@Path("id") id: Int): Response<Character>

    // EPISODES

    @GET("episode")
    suspend fun getEpisodes(): Response<EpisodeResponse>

    @GET("episode/{id}")
    suspend fun  getEpisode(@Path("id") id: Int): Response<Episode>

}