package com.joshdonlan.domain.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.joshdonlan.domain.character.model.Character
import com.joshdonlan.domain.episode.model.Episode

@Database(entities = [Character::class, Episode::class], version = 3, exportSchema = false)
@TypeConverters(Converters::class)
abstract class RickAndMortyDatabase: RoomDatabase() {

    abstract val characterDao: CharacterDao
    abstract val episodeDao: EpisodeDao

    companion object {

        @Volatile
        var INSTANCE: RickAndMortyDatabase? = null

        fun getInstance(context: Context): RickAndMortyDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context,
                        RickAndMortyDatabase::class.java,
                        "rick_and_morty_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }

    }

}