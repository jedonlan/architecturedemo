package com.joshdonlan.domain.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.joshdonlan.domain.character.model.Character

@Dao
interface CharacterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(character: Character)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(characters: List<Character>)

    @Query("SELECT * FROM ${Character.table_name} WHERE id = :characterId")
    fun getCharacterById(characterId: Int): LiveData<Character>

    @Query("SELECT * FROM ${Character.table_name} WHERE id IN (:characterIds)")
    fun getCharactersById(characterIds: List<Int>): LiveData<List<Character>>

    @Query("SELECT * FROM ${Character.table_name}")
    fun getAllCharacters(): LiveData<List<Character>>

}