package com.joshdonlan.domain.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.joshdonlan.domain.episode.model.Episode

@Dao
interface EpisodeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(episode: Episode)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(episode: List<Episode>)

    @Query("SELECT * FROM ${Episode.table_name} WHERE id = :episodeId")
    fun getEpisodeById(episodeId: Int): LiveData<Episode>

    @Query("SELECT * FROM ${Episode.table_name} WHERE id IN (:episodeIds)")
    fun getEpisodesById(episodeIds: List<Int>): LiveData<List<Episode>>

    @Query("SELECT * FROM ${Episode.table_name}")
    fun getAllCharacters(): LiveData<List<Episode>>

}