package com.joshdonlan.domain.episode.model

import com.joshdonlan.domain.common.model.ResponseInfo

data class EpisodeResponse(
    val info: ResponseInfo,
    val results: List<Episode>
)
