package com.joshdonlan.domain.episode

import androidx.lifecycle.asFlow
import com.joshdonlan.coreio.DataAccess
import com.joshdonlan.domain.database.EpisodeDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import javax.inject.Inject

@ExperimentalCoroutinesApi
class EpisodeRepository @Inject constructor(
    private val episodeDataSource: EpisodeDataSource,
    private val episodeDao: EpisodeDao
) {

    fun getEpisodes() = DataAccess.fetch(
        dbQuery = { episodeDao.getAllCharacters() },
        networkCall = { episodeDataSource.getEpisodes() },
        saveCallResult = { episodeDao.insertAll(it.results) }
    ).asFlow().take(3)

}