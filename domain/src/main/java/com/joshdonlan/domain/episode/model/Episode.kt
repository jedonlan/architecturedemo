package com.joshdonlan.domain.episode.model

import androidx.room.Entity
import java.util.Date

@Entity(tableName = Episode.table_name)
data class Episode(
    @androidx.room.PrimaryKey val id: Int,
    val name: String,
    val air_date: String,
    val episode: String,
    val characters: List<String>,
    val url: String,
    val created: Date
) {
    companion object {
        const val table_name = "episode_table"
    }
}

