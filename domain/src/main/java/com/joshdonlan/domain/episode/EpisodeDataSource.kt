package com.joshdonlan.domain.episode

import com.joshdonlan.coreio.BaseDataSource
import com.joshdonlan.domain.api.RickAndMortyService
import javax.inject.Inject

class EpisodeDataSource @Inject constructor(
    private val dataService: RickAndMortyService
): com.joshdonlan.coreio.BaseDataSource() {

    suspend fun getEpisodes() = execute { dataService.getEpisodes() }

}