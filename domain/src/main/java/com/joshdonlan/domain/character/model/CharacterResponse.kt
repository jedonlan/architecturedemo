package com.joshdonlan.domain.character.model

import com.joshdonlan.domain.common.model.ResponseInfo

data class CharacterResponse (
    val info: ResponseInfo,
    val results: List<Character>
)