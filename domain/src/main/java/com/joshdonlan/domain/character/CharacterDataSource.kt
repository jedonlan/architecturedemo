package com.joshdonlan.domain.character

import com.joshdonlan.coreio.BaseDataSource
import com.joshdonlan.domain.api.RickAndMortyService
import javax.inject.Inject

class CharacterDataSource @Inject constructor(
    private val dataService: RickAndMortyService
): com.joshdonlan.coreio.BaseDataSource() {

    suspend fun getCharacter(id: Int) = execute { dataService.getCharacter(id) }

    suspend fun getCharacters(characterIds: List<Int>) = execute { dataService.getCharactersByIds(getCharacterIdString(characterIds)) }

    private fun getCharacterIdString(characterIds: List<Int>): String {
        return characterIds.joinToString(",")
    }

}