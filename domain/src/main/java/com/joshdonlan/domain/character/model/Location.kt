package com.joshdonlan.domain.character.model

data class Location (
    val name: String,
    val url: String
) {

    companion object {
        const val prefix = "location"
    }

}