package com.joshdonlan.domain.character

import androidx.paging.DataSource
import androidx.paging.PagedList
import com.joshdonlan.coreio.BasePagedDataSourceFactory
import com.joshdonlan.domain.character.model.Character

class CharacterDataSourceFactory(private val characterDataSource: CharacterRemoteDataSource): com.joshdonlan.coreio.BasePagedDataSourceFactory<Character>() {

    override fun initConfig(initLoadSize: Int, pageSize: Int, prefetch: Int, placeHolders: Boolean): PagedList.Config {
        return super.initConfig(40, 20, 4, true)
    }

    override fun create(): DataSource<Int, Character> {
        return characterDataSource
    }

}


