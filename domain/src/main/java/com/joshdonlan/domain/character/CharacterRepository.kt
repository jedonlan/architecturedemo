package com.joshdonlan.domain.character

import androidx.lifecycle.asFlow
import com.joshdonlan.coreio.DataAccess
import com.joshdonlan.domain.database.CharacterDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CharacterRepository @Inject constructor(
    private val characterDataSource: CharacterDataSource,
    private val characterDao: CharacterDao
){

    fun getCharacter(id: Int) = DataAccess.fetch(
        dbQuery = { characterDao.getCharacterById(id) },
        networkCall = { characterDataSource.getCharacter(id) },
        saveCallResult = { characterDao.insert(it) }
    ).asFlow().take(3)

    fun getCharacters(characterIds: List<Int>) = com.joshdonlan.coreio.DataAccess.fetch(
        dbQuery = { characterDao.getCharactersById(characterIds) },
        networkCall = { characterDataSource.getCharacters(characterIds) },
        saveCallResult = { characterDao.insertAll(it) }
    ).asFlow().take(3)

}