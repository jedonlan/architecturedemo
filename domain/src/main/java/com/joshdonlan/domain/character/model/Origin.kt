package com.joshdonlan.domain.character.model

data class Origin (
    val name: String,
    val url: String
) {

    companion object {
        const val prefix = "origin"
    }

}