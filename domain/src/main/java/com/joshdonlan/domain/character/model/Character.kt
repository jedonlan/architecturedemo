package com.joshdonlan.domain.character.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = Character.table_name)
data class Character (
    @PrimaryKey val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    @Embedded(prefix = Origin.prefix) val origin: Origin,
    @Embedded(prefix = Location.prefix) val location: Location,
    val image: String,
    val episode: List<String>,
    val url: String,
    val created: Date
) {

    companion object {
        const val table_name = "character_table"
    }

}