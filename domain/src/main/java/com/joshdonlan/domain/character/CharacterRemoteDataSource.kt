package com.joshdonlan.domain.character

import androidx.lifecycle.LifecycleCoroutineScope
import com.joshdonlan.coreio.BasePagedRemoteDataSource
import com.joshdonlan.domain.character.model.Character
import com.joshdonlan.domain.api.RickAndMortyService
import com.joshdonlan.domain.character.model.CharacterResponse
import com.joshdonlan.domain.database.CharacterDao
import javax.inject.Inject

class CharacterRemoteDataSource @Inject constructor(
    private val dataService: RickAndMortyService,
    private val characterDao: CharacterDao,
    lifecycleCoroutineScope: LifecycleCoroutineScope
): BasePagedRemoteDataSource<CharacterResponse, Character>(lifecycleCoroutineScope) {

    override val dataSourceName by lazy { this::class.simpleName ?: "" }

    override val factory = CharacterDataSourceFactory(this)

    override suspend fun fetchInitial() = execute { dataService.getCharacters(1) }

    override suspend fun fetchBefore(page: Int) = execute { dataService.getCharacters(page) }

    override suspend fun fetchAfter(page: Int) = execute { dataService.getCharacters(page) }

    override fun buildPagedResponse(resource: com.joshdonlan.coreio.Resource<CharacterResponse>): PagedResponse<Character> {
        val previous: Int? = resource.data?.info?.prev?.substringAfterLast("=")?.toIntOrNull()
        val next: Int? = resource.data?.info?.next?.substringAfterLast("=")?.toIntOrNull()
        val data = resource.data?.results ?: listOf()
        return PagedResponse(com.joshdonlan.coreio.Resource.success(data), previous, next)
    }

    override suspend fun storeResults(data: List<Character>) {
        characterDao.insertAll(data)
    }

}
