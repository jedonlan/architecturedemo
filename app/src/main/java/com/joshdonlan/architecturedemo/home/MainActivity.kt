package com.joshdonlan.architecturedemo.home

import androidx.activity.viewModels
import androidx.viewbinding.ViewBinding
import com.joshdonlan.architecturedemo.character.CharacterActivity
import com.joshdonlan.architecturedemo.databinding.ActivityMainBinding
import com.joshdonlan.architecturedemo.episode.EpisodeActivity
import com.joshdonlan.architecturedemo.home.view.MainView
import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.annotation.WithViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import reactivecircus.flowbinding.android.view.clicks

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithViewModel(MainViewModel::class)
class MainActivity : BaseActivity<MainView, MainViewModel>(), MainView {

    override val viewModel: MainViewModel by viewModels()

    override val binding by lazy { initViewBinding() as ActivityMainBinding }

    override fun initViewBinding(): ViewBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun getCharacterClicks(): Flow<Unit> {
        return binding.charactersButton.clicks()
    }

    override fun getEpisodeClicks(): Flow<Unit> {
        return binding.episodesButton.clicks()
    }


    @FlowPreview
    override fun navigateToCharacters() {
        CharacterActivity.start(this)
    }

    override fun navigateToEpisodes() {
        EpisodeActivity.start(this)
    }

}