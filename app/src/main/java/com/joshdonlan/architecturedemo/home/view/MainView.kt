package com.joshdonlan.architecturedemo.home.view

import kotlinx.coroutines.flow.Flow

interface MainView {

    fun getCharacterClicks(): Flow<Unit>
    fun getEpisodeClicks(): Flow<Unit>
    fun navigateToCharacters()
    fun navigateToEpisodes()

}