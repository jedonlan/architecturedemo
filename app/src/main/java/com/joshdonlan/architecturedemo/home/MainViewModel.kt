package com.joshdonlan.architecturedemo.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.joshdonlan.architecturedemo.home.view.MainView
import com.joshdonlan.core.BaseViewModel
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.core.lifecycle.StateBundle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


@WithView(MainView::class)
class MainViewModel @ViewModelInject constructor(): BaseViewModel<MainView>() {


    override fun present(bundle: StateBundle) {
        super.present(bundle)
        observeNavigation()
    }

    private fun observeNavigation() {
        viewModelScope.launch(Dispatchers.Main) {
            view.getCharacterClicks()
                .collect { handleCharacterClicks() }
        }

        viewModelScope.launch(Dispatchers.Main) {
            view.getEpisodeClicks()
                .collect { handleEpisodeClicks() }
        }
    }

    private fun handleCharacterClicks() {
        view.navigateToCharacters()
    }

    private fun handleEpisodeClicks() {
        view.navigateToEpisodes()
    }

}