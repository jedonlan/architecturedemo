package com.joshdonlan.architecturedemo.character.card

import com.joshdonlan.core.BaseDelegatePresenter
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.coreio.Resource
import com.joshdonlan.domain.character.CharacterRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
@WithView(CharacterCardView::class)
class CharacterCardPresenter @Inject constructor(
    private val characterRepository: CharacterRepository
): BaseDelegatePresenter<CharacterCardView>() {

    private var currentCharacter: Character? = null

    fun present(coroutineScope: CoroutineScope) {
        this.coroutineScope = coroutineScope

        showCharacter(currentCharacter)
        observeCloseClicks()
    }

    private fun observeCloseClicks() {
        coroutineScope?.launch(Dispatchers.Main) {
            view.getCharacterCardCloseClicks()
                .collect { hideCharacter() }
        }
    }

    fun showCharacter(character: Character?) {
        setCharacterAndShow(character)
    }

    fun showCharacter(id: Int) {
        coroutineScope?.launch(Dispatchers.Main) {
            characterRepository.getCharacter(id)
                .flowOn(Dispatchers.IO)
                .filter { it.status == Resource.Status.SUCCESS }
                .collect { setCharacterAndShow(it.data) }
        }
    }

    private fun setCharacterAndShow(character: Character?) {
        currentCharacter = character
        view.showCharacter(currentCharacter)
    }

    private fun hideCharacter() {
        showCharacter(null)
    }

}