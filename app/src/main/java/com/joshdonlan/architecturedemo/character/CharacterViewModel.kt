package com.joshdonlan.architecturedemo.character

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.joshdonlan.architecturedemo.character.adapter.CharacterPagedAdapter
import com.joshdonlan.architecturedemo.character.card.CharacterCardPresenter
import com.joshdonlan.architecturedemo.character.view.CharacterView
import com.joshdonlan.core.BaseViewModel
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.core.lifecycle.StateBundle
import com.joshdonlan.domain.character.CharacterRemoteDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@FlowPreview
@ExperimentalCoroutinesApi
@WithView(CharacterView::class)
class CharacterViewModel @ViewModelInject constructor(
    private val characterCardPresenter: CharacterCardPresenter,
    private val characterDataSource: CharacterRemoteDataSource
): BaseViewModel<CharacterView>() {

    private val characterAdapter by lazy { CharacterPagedAdapter(viewModelScope) }

    override fun presentOnce(bundle: StateBundle) {
        addDelegatePresenter(characterCardPresenter)
        initCharacterAdapter()
    }

    override fun presentAlways(bundle: StateBundle) {
        characterCardPresenter.present(viewModelScope)
        view.setCharacterAdapter(characterAdapter)
    }

    private fun initCharacterAdapter() {
        this.viewModelScope.launch(Dispatchers.Main) {
            characterDataSource.pagedListFlow
                .flowOn(Dispatchers.IO)
                .collect { characterList -> characterAdapter.submitList(characterList) }
        }
        observeCharacterClicks()
    }

    private fun observeCharacterClicks() {
        viewModelScope.launch(Dispatchers.Main) {
            characterAdapter.getItemClicksFlow()
                .collect { character -> characterCardPresenter.showCharacter(character) }
        }
    }

}