package com.joshdonlan.architecturedemo.character.card

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.joshdonlan.architecturedemo.R
import com.joshdonlan.architecturedemo.databinding.LayoutCharacterCardBinding
import com.joshdonlan.common.Constants.EMPTY_STR
import com.joshdonlan.common.GlideApp
import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.flow.Flow
import reactivecircus.flowbinding.android.view.clicks

class CharacterCard(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs), CharacterCardView {

    private val binding by lazy { LayoutCharacterCardBinding.inflate(LayoutInflater.from(context), parent as ViewGroup) }

    override fun showCharacter(character: Character?) {
        if (character == null) {
            binding.characterCardBackground.setOnClickListener {}
            binding.cardCharacterName.text = EMPTY_STR
            binding.cardCharacterLocation.text = EMPTY_STR
            binding.cardCharacterImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_character))
            binding.characterCardViews.visibility = View.GONE
        } else {
            val uri = character.image.toUri().buildUpon().scheme("https").build()
            GlideApp.with(context)
                .load(uri)
                .placeholder(R.drawable.ic_character)
                .error(R.drawable.ic_character)
                .into(binding.cardCharacterImage)

            binding.cardCharacterName.text = character.name
            binding.cardCharacterLocation.text = character.location.name
            binding.characterCardViews.visibility = View.VISIBLE
        }
    }

    override fun getCharacterCardCloseClicks(): Flow<Unit> {
        return binding.characterCardScrim.clicks()
    }

}