package com.joshdonlan.architecturedemo.character.view

import com.joshdonlan.architecturedemo.character.adapter.CharacterPagedAdapter
import com.joshdonlan.architecturedemo.character.card.CharacterCardView
import kotlinx.coroutines.ExperimentalCoroutinesApi

interface CharacterView: CharacterCardView {

    @ExperimentalCoroutinesApi
    fun setCharacterAdapter(adapter: CharacterPagedAdapter)

}