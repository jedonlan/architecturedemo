package com.joshdonlan.architecturedemo.character.adapter

import android.view.ViewGroup
import com.joshdonlan.core.BasePagedListAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.joshdonlan.domain.character.model.Character

@ExperimentalCoroutinesApi
class CharacterPagedAdapter(private val scope: CoroutineScope): BasePagedListAdapter<Character, CharacterViewHolder>(CharacterDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
        holder.observeClicks(itemClicks, scope)
    }

}