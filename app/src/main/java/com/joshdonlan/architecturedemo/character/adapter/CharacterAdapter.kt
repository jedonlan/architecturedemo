package com.joshdonlan.architecturedemo.character.adapter

import android.view.ViewGroup
import com.joshdonlan.core.BaseAdapter
import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class CharacterAdapter(
    characterList: ArrayList<Character>,
    private val scope: CoroutineScope
): BaseAdapter<Character, CharacterViewHolder>(characterList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.observeClicks(itemClicks, scope)
    }

}