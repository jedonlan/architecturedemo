package com.joshdonlan.architecturedemo.character.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.net.toUri
import com.joshdonlan.architecturedemo.R
import com.joshdonlan.architecturedemo.databinding.ItemCharacterBinding
import com.joshdonlan.common.GlideApp
import com.joshdonlan.core.BaseViewHolder
import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.view.clicks

@ExperimentalCoroutinesApi
class CharacterViewHolder(private val binding: ItemCharacterBinding): BaseViewHolder<Character>(binding.root) {

    override fun bind(model: Character?) {
        super.bind(model)
        model?.also {
            setCharacterImage(binding.characterImage, it.image)
            binding.characterName.text = it.name
            binding.characterSpecies.text = it.species
            binding.characterStatus.text = it.status
        }
    }

    override fun observeClicks(channel: BroadcastChannel<Character>, scope: CoroutineScope) {
        binding.root.clicks()
            .onEach { channel.send(item) }
            .launchIn(scope)
    }

    private fun setCharacterImage(view: ImageView, image: String) {
        val uri = image.toUri().buildUpon().scheme("https").build()
        GlideApp
            .with(view.context)
            .load(uri)
            .placeholder(R.drawable.ic_character)
            .error(R.drawable.ic_character)
            .into(view)
    }

    companion object {
        fun from(parent: ViewGroup): CharacterViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemCharacterBinding.inflate(layoutInflater, parent, false)

            return CharacterViewHolder(binding)
        }
    }

}