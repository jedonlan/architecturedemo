package com.joshdonlan.architecturedemo.character

import android.content.Context
import android.content.Intent
import androidx.activity.viewModels
import androidx.viewbinding.ViewBinding
import com.joshdonlan.architecturedemo.databinding.ActivityCharacterBinding
import com.joshdonlan.architecturedemo.character.adapter.CharacterPagedAdapter
import com.joshdonlan.architecturedemo.character.view.CharacterView
import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.annotation.WithViewModel
import com.joshdonlan.domain.character.model.Character
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow

@AndroidEntryPoint
@FlowPreview
@ExperimentalCoroutinesApi
@WithViewModel(CharacterViewModel::class)
class CharacterActivity: BaseActivity<CharacterView, CharacterViewModel>(), CharacterView {

    override val viewModel: CharacterViewModel by viewModels()

    override val binding by lazy { initViewBinding() as ActivityCharacterBinding }

    override fun initViewBinding(): ViewBinding {
        return ActivityCharacterBinding.inflate(layoutInflater)
    }

    override fun setCharacterAdapter(adapter: CharacterPagedAdapter) {
        binding.characterList.adapter = adapter
    }

    override fun showCharacter(character: Character?) {
        binding.characterCard.showCharacter(character)
    }

    override fun getCharacterCardCloseClicks(): Flow<Unit> {
        return binding.characterCard.getCharacterCardCloseClicks()
    }

    companion object {

        @JvmStatic
        fun start(context: Context) {
            Intent(context, CharacterActivity::class.java).also {
                context.startActivity(it)
            }
        }

    }

}