package com.joshdonlan.architecturedemo.character.adapter

import androidx.recyclerview.widget.DiffUtil
import com.joshdonlan.domain.character.model.Character
import timber.log.Timber

class CharacterDiffCallback: DiffUtil.ItemCallback<Character>() {
    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        Timber.v("itemcheck - oldItem: ${oldItem.id} - ${oldItem.name} vs newItem: ${newItem.id} - ${newItem.name}")
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        Timber.v("contents check - oldItem: ${oldItem.id} - ${oldItem.name} vs newItem: ${newItem.id} - ${newItem.name}")
        return oldItem.name == newItem.name && oldItem.species == newItem.species && oldItem.image == newItem.image
    }
}