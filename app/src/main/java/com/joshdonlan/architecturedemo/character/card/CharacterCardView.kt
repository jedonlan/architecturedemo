package com.joshdonlan.architecturedemo.character.card

import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.flow.Flow

interface CharacterCardView {
    fun showCharacter(character: Character?)
    fun getCharacterCardCloseClicks(): Flow<Unit>
}