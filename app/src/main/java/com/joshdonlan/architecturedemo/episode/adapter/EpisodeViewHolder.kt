package com.joshdonlan.architecturedemo.episode.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.joshdonlan.architecturedemo.databinding.ItemEpisodeBinding
import com.joshdonlan.core.BaseViewHolder
import com.joshdonlan.domain.episode.model.Episode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.view.clicks

@ExperimentalCoroutinesApi
class EpisodeViewHolder(private val binding: ItemEpisodeBinding): BaseViewHolder<Episode>(binding.root) {

    override fun bind(model: Episode?) {
        super.bind(model)
        model?.also {
            binding.episodeName.text = it.name
            binding.episodeDate.text = it.air_date
        }
    }

    override fun observeClicks(channel: BroadcastChannel<Episode>, scope: CoroutineScope) {
        binding.root.clicks()
            .onEach { channel.send(item) }
            .launchIn(scope)
    }

    companion object {
        fun from(parent: ViewGroup): EpisodeViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemEpisodeBinding.inflate(layoutInflater, parent, false)

            return EpisodeViewHolder(binding)
        }
    }

}