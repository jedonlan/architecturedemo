package com.joshdonlan.architecturedemo.episode.card

import com.joshdonlan.architecturedemo.character.adapter.CharacterAdapter
import com.joshdonlan.architecturedemo.character.card.CharacterCardPresenter
import com.joshdonlan.core.BaseDelegatePresenter
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.coreio.Resource
import com.joshdonlan.domain.character.CharacterRepository
import com.joshdonlan.domain.character.model.Character
import com.joshdonlan.domain.episode.model.Episode
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
@WithView(EpisodeCardView::class)
class EpisodeCardPresenter @Inject constructor(
    private val characterRepository: CharacterRepository,
    private val characterCardPresenter: CharacterCardPresenter
): BaseDelegatePresenter<EpisodeCardView>() {

    private lateinit var characterAdapter: CharacterAdapter

    private var characterList = arrayListOf<Character>()
    private var currentEpisode: Episode? = null

    fun present(coroutineScope: CoroutineScope) {
        this.coroutineScope = coroutineScope

        addDelegatePresenter(characterCardPresenter)
        characterCardPresenter.present(coroutineScope)

        initCharacterList(coroutineScope)
        showEpisode(currentEpisode)
        observeCloseClicks()
    }

    fun showEpisode(episode: Episode?) {
        currentEpisode = episode
        if (episode == null) {
            view.closeEpisodeCard()
        } else {
            initEpisode(episode)
        }
    }

    override fun viewAttached() {
        super.viewAttached()
        view.setCharacterAdapter(characterAdapter)
        observeCharacterClicks()
    }

    private fun initCharacterList(coroutineScope: CoroutineScope) {
        characterAdapter = CharacterAdapter(characterList, coroutineScope)
    }

    private fun initEpisode(episode: Episode?) {
        episode?.also {
            view.setEpisodeData(episode.name, episode.air_date, episode.url)
            populateCharacterList()
        }
        view.showEpisodeCard()
    }

    private fun populateCharacterList() {
        val characterIds: MutableList<Int> = mutableListOf()
        currentEpisode?.characters?.forEach {
            val id = it.substringAfterLast("/").toInt()
            characterIds.add(id)
        }
        coroutineScope?.launch(Dispatchers.Main) {
            characterRepository.getCharacters(characterIds)
                .filter { it.status == com.joshdonlan.coreio.Resource.Status.SUCCESS }
                .flowOn(Dispatchers.IO)
                .collect {
                    Timber.v("Loading ${it.data?.count()} characters")
                    handleCharacterResponse(it.data)
                }
        }
    }

    private fun handleCharacterResponse(characters: List<Character>?) {
        characters?.also {
            characterList.clear()
            characterList.addAll(it)
            characterAdapter.notifyDataSetChanged()
        }
    }

    private fun observeCloseClicks() {
        coroutineScope?.launch(Dispatchers.Main) {
            view.getEpisodeCardCloseClicks()
                .collect { showEpisode(null) }
        }
    }

    private fun observeCharacterClicks() {
        coroutineScope?.launch(Dispatchers.Main) {
            characterAdapter.getItemClicksFlow()
                .collect { characterCardPresenter.showCharacter(it) }
        }
    }

}