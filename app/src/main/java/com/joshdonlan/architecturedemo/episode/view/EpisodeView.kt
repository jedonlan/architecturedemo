package com.joshdonlan.architecturedemo.episode.view

import com.joshdonlan.architecturedemo.episode.adapter.EpisodeAdapter
import com.joshdonlan.architecturedemo.episode.card.EpisodeCardView
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
interface EpisodeView: EpisodeCardView {
    fun setEpisodeAdapter(episodeAdapter: EpisodeAdapter)
}