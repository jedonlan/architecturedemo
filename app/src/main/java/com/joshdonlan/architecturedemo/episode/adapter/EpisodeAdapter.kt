package com.joshdonlan.architecturedemo.episode.adapter

import android.view.ViewGroup
import com.joshdonlan.core.BaseAdapter
import com.joshdonlan.domain.episode.model.Episode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class EpisodeAdapter(
    episodeList: ArrayList<Episode>,
    private val scope: CoroutineScope
): BaseAdapter<Episode, EpisodeViewHolder>(episodeList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.observeClicks(itemClicks, scope)
    }

}