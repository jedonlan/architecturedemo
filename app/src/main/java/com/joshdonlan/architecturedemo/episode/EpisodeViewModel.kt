package com.joshdonlan.architecturedemo.episode

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.joshdonlan.architecturedemo.episode.adapter.EpisodeAdapter
import com.joshdonlan.architecturedemo.episode.card.EpisodeCardPresenter
import com.joshdonlan.architecturedemo.episode.view.EpisodeView
import com.joshdonlan.core.BaseViewModel
import com.joshdonlan.core.annotation.WithView
import com.joshdonlan.core.lifecycle.StateBundle
import com.joshdonlan.coreio.Resource
import com.joshdonlan.domain.episode.EpisodeRepository
import com.joshdonlan.domain.episode.model.Episode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
@WithView(EpisodeView::class)
class EpisodeViewModel @ViewModelInject constructor(
    private val episodeRepository: EpisodeRepository,
    private val episodeCardPresenter: EpisodeCardPresenter
): BaseViewModel<EpisodeView>() {

    private var episodeList = arrayListOf<Episode>()

    private val episodeAdapter = EpisodeAdapter(episodeList, viewModelScope)

    override fun presentOnce(bundle: StateBundle) {
        initEpisodeAdapter()
        addDelegatePresenter(episodeCardPresenter)
    }

    override fun presentAlways(bundle: StateBundle) {
        episodeCardPresenter.present(viewModelScope)
        view.setEpisodeAdapter(episodeAdapter)
    }

    private fun initEpisodeAdapter() {
        viewModelScope.launch(Dispatchers.Main) {
            episodeRepository.getEpisodes()
                .filter { it.status == com.joshdonlan.coreio.Resource.Status.SUCCESS }
                .flowOn(Dispatchers.IO)
                .collect { handleEpisodeResponse(it.data) }
        }
        viewModelScope.launch(Dispatchers.Main) {
            episodeAdapter.getItemClicksFlow()
                .collectLatest {
                    Timber.v("Collected from EpisodeAdapter ${it.episode}")
                    episodeCardPresenter.showEpisode(it)
                }
        }
    }

    private fun handleEpisodeResponse(episodes: List<Episode>?) {
        episodes?.also {
            this.episodeList.clear()
            this.episodeList.addAll(episodes)
            episodeAdapter.notifyDataSetChanged()
        }
    }

}