package com.joshdonlan.architecturedemo.episode

import android.content.Context
import android.content.Intent
import androidx.activity.viewModels
import androidx.viewbinding.ViewBinding
import com.joshdonlan.architecturedemo.character.adapter.CharacterAdapter
import com.joshdonlan.architecturedemo.databinding.ActivityEpisodeBinding
import com.joshdonlan.architecturedemo.episode.adapter.EpisodeAdapter
import com.joshdonlan.architecturedemo.episode.view.EpisodeView
import com.joshdonlan.core.BaseActivity
import com.joshdonlan.core.annotation.WithViewModel
import com.joshdonlan.domain.character.model.Character
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithViewModel(EpisodeViewModel::class)
class EpisodeActivity: BaseActivity<EpisodeView, EpisodeViewModel>(), EpisodeView {

    override val viewModel: EpisodeViewModel by viewModels()

    override val binding by lazy { initViewBinding() as ActivityEpisodeBinding }

    override fun initViewBinding(): ViewBinding {
        return ActivityEpisodeBinding.inflate(layoutInflater)
    }

    override fun setEpisodeAdapter(episodeAdapter: EpisodeAdapter) {
        binding.episodeList.adapter = episodeAdapter
    }

    override fun setEpisodeData(name: String, airDate: String, url: String) {
        binding.episodeCard.setEpisodeData(name, airDate, url)
    }

    override fun setCharacterAdapter(characterAdapter: CharacterAdapter) {
        binding.episodeCard.setCharacterAdapter(characterAdapter)
    }

    override fun showEpisodeCard() {
        binding.episodeCard.showEpisodeCard()
    }

    override fun closeEpisodeCard() {
        binding.episodeCard.closeEpisodeCard()
    }

    override fun getEpisodeCardCloseClicks(): Flow<Unit> {
        return binding.episodeCard.getEpisodeCardCloseClicks()
    }

    override fun showCharacter(character: Character?) {
        binding.episodeCard.showCharacter(character)
    }

    override fun getCharacterCardCloseClicks(): Flow<Unit> {
        return binding.episodeCard.getCharacterCardCloseClicks()
    }

    companion object {

        @JvmStatic
        fun start(context: Context) {
            Intent(context, EpisodeActivity::class.java).also {
                context.startActivity(it)
            }
        }

    }

}