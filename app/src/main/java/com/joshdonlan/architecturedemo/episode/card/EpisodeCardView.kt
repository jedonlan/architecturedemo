package com.joshdonlan.architecturedemo.episode.card

import com.joshdonlan.architecturedemo.character.adapter.CharacterAdapter
import com.joshdonlan.architecturedemo.character.card.CharacterCardView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface EpisodeCardView: CharacterCardView {
    fun setEpisodeData(name: String, airDate: String, url: String)
    fun setCharacterAdapter(characterAdapter: CharacterAdapter)
    fun showEpisodeCard()
    fun closeEpisodeCard()
    fun getEpisodeCardCloseClicks(): Flow<Unit>
}