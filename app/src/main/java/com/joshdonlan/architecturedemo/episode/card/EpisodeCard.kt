package com.joshdonlan.architecturedemo.episode.card

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.joshdonlan.architecturedemo.character.adapter.CharacterAdapter
import com.joshdonlan.architecturedemo.databinding.LayoutEpisodeCardBinding
import com.joshdonlan.domain.character.model.Character
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import reactivecircus.flowbinding.appcompat.navigationClicks

@ExperimentalCoroutinesApi
class EpisodeCard(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs), EpisodeCardView {

    private val binding by lazy { LayoutEpisodeCardBinding.inflate(LayoutInflater.from(context), parent as ViewGroup) }

    override fun setEpisodeData(name: String, airDate: String, url: String) {
        binding.episodeCardToolbar.title = name
        binding.episodeCardToolbar.subtitle = airDate
        binding.episodeCardUrl.text = url
    }

    override fun setCharacterAdapter(characterAdapter: CharacterAdapter) {
        binding.episodeCardCharacterList.adapter = characterAdapter
    }

    override fun showEpisodeCard() {
        binding.episodeCardViews.visibility = View.VISIBLE
    }

    override fun closeEpisodeCard() {
        binding.episodeCardViews.visibility = View.GONE
    }

    override fun getEpisodeCardCloseClicks(): Flow<Unit> {
        return binding.episodeCardToolbar.navigationClicks()
    }

    override fun showCharacter(character: Character?) {
        binding.characterCard.showCharacter(character)
    }

    override fun getCharacterCardCloseClicks(): Flow<Unit> {
        return binding.characterCard.getCharacterCardCloseClicks()
    }
}