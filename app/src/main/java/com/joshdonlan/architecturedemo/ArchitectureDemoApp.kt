package com.joshdonlan.architecturedemo

import com.joshdonlan.core.BaseApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArchitectureDemoApp: BaseApplication()